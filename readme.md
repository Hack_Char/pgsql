Build a minimized PostgreSQL Container
======================================

These instructions assume the repository is cloned into a 'pgsql' directory.

Run:
```
docker-compose up -d pgsql-strace
```

This will launch a basic Ubuntu 20.04 container with PostgreSQL running wrapped by strace.
You will need to exercise this container with whatever commands you expect to use later. 
This ensures the strace process is able to capture all the files needed to run the container.

For example:
```
psql -h localhost -U postgres -W
\du postgres
\dS+
\q
```
If you didn't set the password, it defaults to 'postgres'

After this, properly shutdown the database:
```
docker exec -it pgsql_pgsql-strace_1 /usr/bin/pg_ctlcluster 12 main stop
```
This should stop the container.

Run the parse script:
```
./parse.sh
```
This will commmit the strace container as a build image, get the strace.txt file that shows all the file syscalls, parse this file to generate a list of files needed and generate a script that goes in the 'min' directory called 'strace.sh'

Next start up the 'min'imized PostgresSQL container
```
docker-compose up -d pgsql
```
You should now have a minimized PostgreSQL container.

Testing showed the Ubuntu based image was 395MB big, the straced build container 438MB and the minimized container 111MB.



