#!/bin/bash

docker cp pgsql_pgsql-strace_1:/strace.txt .
OUT_FILE=min/strace.sh
INCLUDE_DIRS=/var/lib/postgresql/12/main

# need to put the current working directory as the first item on each line to track where we chdir to
DATA_OUTPUT=""
DATA_CREAT=""
DATA_OPEN=""
CURDIR="/"
while read i ; do
	if [[ "$i" != *"ENOENT"* ]]; then
		if [[ "$i" == *"chdir"* ]]; then
			CURDIR=`echo "$i" | cut -f2 -d\\"`
		else
			# parse exec
			if [[ "$i" == *"execve"* ]]; then
				PARSE="`echo $i | cut -s -f2 -d \\"`"
				if [[ "$PARSE" != "/"* ]]; then
					PARSE="${CURDIR}/$PARSE"
				fi
				if [[ "$DATA_OUTPUT" != *"$PARSE"* ]]; then
					DATA_OUTPUT="$DATA_OUTPUT $PARSE"
				fi
			elif [[ "$i" == *"readlink"* ]]; then
				PARSE="`echo $i | cut -s -f4 -d \\"`"
				if [[ "$PARSE" != "/"* ]]; then
					PARSE="${CURDIR}/$PARSE"
				fi
				if [[ "$DATA_OUTPUT" != *"$PARSE"* ]]; then
					DATA_OUTPUT="$DATA_OUTPUT $PARSE"
				fi
			elif [[ "$i" == *"stat"* ]]; then
				PARSE="`echo $i | cut -s -f2 -d \\"`"
				if [[ "$PARSE" != "/"* ]]; then
					PARSE="${CURDIR}/$PARSE"
				fi
				if [[ "$DATA_OUTPUT" != *"$PARSE"* ]]; then
					DATA_OUTPUT="$DATA_OUTPUT $PARSE"
				fi
			elif [[ "$i" == *"O_CREAT"* ]]; then
				PARSE="`echo $i | cut -s -f2 -d \\"`"
				if [[ "$PARSE" != "/"* ]]; then
					PARSE="${CURDIR}/$PARSE"
				fi
				if [[ "$DATA_CREAT" != *"$PARSE"* ]]; then
					DATA_CREAT="$DATA_CREAT $PARSE"
				fi
			elif [[ "$i" == *"openat"* ]] && [[ "$i" != *"/dev"* ]] && [[ "$i" != *"/proc"* ]] && [[ "$i" != *"/sys"* ]];  then
				PARSE="`echo $i | cut -s -f2 -d \\"`"
				if [[ "$PARSE" != "/"* ]]; then
					PARSE="${CURDIR}/$PARSE"
				fi
				if [[ "$DATA_OPEN" != *"$PARSE"* ]]; then
					DATA_OPEN="$DATA_OPEN $PARSE"
				fi
			fi
		fi
	fi
done < "strace.txt"


# include all files created during the strace, but grabbing from original strace image
DATA_OUTPUT="${DATA_OUTPUT} ${DATA_CREAT} ${DATA_OPEN}"

echo "#!/bin/bash" > $OUT_FILE
echo "" >> $OUT_FILE
echo "mkdir -p \$1" >> $OUT_FILE
echo "mkdir -p \$1/lib/x86_64-linux-gnu" >> $OUT_FILE
echo "mkdir -p \$1/lib64" >> $OUT_FILE
echo "cp -a /lib64/ld* \$1/lib64" >> $OUT_FILE
echo "cp -a /lib/x86_64-linux-gnu/ld* \$1/lib/x86_64-linux-gnu" >> $OUT_FILE
echo "for f in $DATA_OUTPUT ; do " >> $OUT_FILE
echo "	if [ -a \$f ]; then " >> $OUT_FILE
echo "		DIR_NAME=\"\" " >> $OUT_FILE
echo "		if [ -d \$f ]; then " >> $OUT_FILE
echo "			DIR_NAME=\$f" >> $OUT_FILE
echo "		else " >> $OUT_FILE
echo "			DIR_NAME=\`dirname \$f\`" >> $OUT_FILE
echo "		fi" >> $OUT_FILE
echo "		if [ ! -d \${1}\$DIR_NAME ]; then" >> $OUT_FILE
echo "			mkdir -p \${1}\$DIR_NAME" >> $OUT_FILE
echo "			while [[ \$DIR_NAME != '/' ]]; do " >> $OUT_FILE
echo "				chown --reference \$DIR_NAME \${1}\$DIR_NAME" >> $OUT_FILE
echo "				chmod --reference \$DIR_NAME \${1}\$DIR_NAME" >> $OUT_FILE
echo "				DIR_NAME=\`dirname \$DIR_NAME\`" >> $OUT_FILE
echo "			done" >> $OUT_FILE
echo "		fi" >> $OUT_FILE
echo "		if [ ! -d \$f ]; then" >> $OUT_FILE
echo "			if [ -L \$f ]; then" >> $OUT_FILE
echo "				LINK_NAME=\`readlink -f \$f\`" >> $OUT_FILE
echo "				cp -a \$LINK_NAME \${1}\$LINK_NAME" >> $OUT_FILE
echo "				ln -s \$LINK_NAME \${1}\$f" >> $OUT_FILE
echo "			else" >> $OUT_FILE
echo "				cp -a \$f \${1}\$f" >> $OUT_FILE
echo "			fi" >> $OUT_FILE
echo "		fi" >> $OUT_FILE
echo "	fi " >> $OUT_FILE
echo "done" >> $OUT_FILE

# override some dirs
# not practical to touch all the database files, just include them
echo "INCLUDE_DIRS=${INCLUDE_DIRS}" >> $OUT_FILE
echo "for f in \$INCLUDE_DIRS ; do" >> $OUT_FILE
echo "	mkdir -p \${1}\`dirname \$f\` " >> $OUT_FILE
echo "	cp -a \$f \${1}\`dirname \$f\` " >> $OUT_FILE
echo "done" >> $OUT_FILE

echo "exit 0" >> $OUT_FILE

